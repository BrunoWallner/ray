use crate::{
    ray::{Hit, Hittable, Ray},
    Material,
};
use glam::Vec3;

pub struct Sphere {
    radius: f32,
    center: Vec3,
    material: Material,
}
impl Sphere {
    pub fn new(center: Vec3, radius: f32, material: Material) -> Self {
        Self {
            radius,
            center,
            material,
        }
    }
}
impl Hittable for Sphere {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<Hit> {
        let oc = ray.origin() - self.center;
        let a = ray.direction().length_squared();
        let half_b = oc.dot(ray.direction());
        let c = oc.length_squared() - self.radius * self.radius;
        let discriminant = half_b * half_b - a * c;

        if discriminant < 0.0 {
            return None;
        };

        let sqrtd = discriminant.sqrt();

        let mut root = (-half_b - sqrtd) / a;
        if root < t_min || root > t_max {
            root = (-half_b + sqrtd) / a;
            if root < t_min || root > t_max {
                return None;
            }
        }

        let t = root;
        let position = ray.at(t);
        let normal = (position - self.center) / self.radius;
        let front = ray.direction().dot(normal) <= 0.0;
        let material = self.material;

        return Some(Hit {
            t,
            position,
            normal,
            front,
            material,
        });
    }
}

pub struct Plane {
    size: f32,
    center: Vec3,
    normal: Vec3,
    material: Material,
}
impl Plane {
    pub fn new(center: Vec3, size: f32, normal: Vec3, material: Material) -> Self {
        let normal = normal.normalize();
        Self {
            size,
            center,
            normal,
            material,
        }
    }
}

impl Hittable for Plane {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<Hit> {
        let denominator = ray.direction.dot(self.normal);
        if denominator <= f32::EPSILON {
            return None;
        } else {
            let p0 = self.center - ray.origin;

            let t = p0.dot(self.normal) / denominator;
            if t < t_min || t > t_max {
                return None;
            }

            let position = ray.at(t);
            let front = t >= 0.0;
            let material = self.material;
            let normal = -self.normal;

            // check distance
            let rp = position - self.center;
            let in_distance = rp.x < self.size
                && rp.x > -self.size
                && rp.y < self.size
                && rp.y > -self.size
                && rp.z < self.size
                && rp.z > -self.size;

            if in_distance {
                return Some(Hit {
                    t,
                    position,
                    normal,
                    front,
                    material,
                });
            } else {
                return None;
            }
        }
    }
}
