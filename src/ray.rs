use crate::material::Material;
use glam::Vec3;

#[derive(Copy, Clone, PartialEq)]
pub struct Ray {
    pub origin: Vec3,
    pub direction: Vec3,
}
impl Ray {
    pub fn new(origin: Vec3, direction: Vec3) -> Self {
        Self { origin, direction }
    }

    pub fn origin(&self) -> Vec3 {
        self.origin
    }

    pub fn direction(&self) -> Vec3 {
        self.direction
    }

    pub fn at(&self, t: f32) -> Vec3 {
        self.origin + (t * self.direction)
    }
}

pub struct Hit {
    pub position: Vec3,
    pub normal: Vec3,
    pub t: f32,
    pub front: bool,
    pub material: Material,
}

pub trait Hittable {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<Hit>;
}

pub struct World {
    hittables: Vec<Box<dyn Hittable + Send + Sync>>,
}

impl World {
    pub fn new() -> Self {
        Self {
            hittables: Vec::new(),
        }
    }

    pub fn add(&mut self, hittable: impl Hittable + Send + Sync + 'static) {
        self.hittables.push(Box::new(hittable));
    }

    pub fn clear(&mut self) {
        self.hittables.clear();
    }
}
impl Hittable for World {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<Hit> {
        let mut hit_record: Option<Hit> = None;

        for hittable in &self.hittables {
            if let Some(hit) = hittable.hit(ray, t_min, t_max) {
                if !hit.front {
                    continue;
                }
                if let Some(hit_record) = &mut hit_record {
                    if hit.t < hit_record.t {
                        *hit_record = hit;
                    }
                } else {
                    hit_record = Some(hit);
                }
            }
        }

        hit_record
    }
}
