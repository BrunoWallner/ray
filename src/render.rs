use std::f32::EPSILON;
use std::sync::Arc;
use std::sync::Mutex;

use crate::Camera;
use crate::Ray;
use glam::Vec3;
use macroquad::prelude::Color;
use macroquad::texture::Image;
use rand::{thread_rng, Rng};
use rayon::prelude::IntoParallelIterator;
use rayon::prelude::ParallelIterator;

const RAY_DEPTH: usize = 8;
const SAMPLES_PER_PIXEL: u32 = 1;
const BLUR: f32 = 1.0;
const SKY_COLOR: [Vec3; 2] = [Vec3::new(0.75, 0.4, 0.65), Vec3::new(0.4, 0.6, 0.9)];

use crate::Hittable;
use crate::World;

pub struct Renderer {
    pub world: World,
    pub camera: Camera,
    pub data: Vec<f32>,

    width: u32,
    height: u32,
}
impl Renderer {
    pub fn new(world: World, camera: Camera, width: u32, height: u32) -> Self {
        let data = vec![0.0; width as usize * height as usize * 3];
        Self {
            world,
            camera,
            width,
            height,
            data,
        }
    }

    pub fn set_dimension(&mut self, width: u32, height: u32) {
        self.width = width;
        self.height = height;
        self.data = vec![0.0; width as usize * height as usize * 3];
    }

    pub fn render(&mut self) {
        let img: Arc<Mutex<Vec<f32>>> = Arc::new(Mutex::new(vec![
            0.0;
            self.width as usize
                * self.height as usize
                * 3
        ]));

        let colums = (0..self.width).collect::<Vec<u32>>();
        colums.into_par_iter().for_each(|x| {
            let mut rng = thread_rng();

            // let mut color_column = vec![Vec3::ZERO; image_height as usize];
            let mut color_column = vec![0.0; self.height as usize * 3];
            for y in 0..self.height {
                let mut pixel_color = Vec3::splat(0.0);
                for _ in 0..SAMPLES_PER_PIXEL {
                    let u: f32 =
                        (x as f32 + (rng.gen::<f32>() - 0.5) * BLUR) / (self.width - 1) as f32;
                    let v: f32 =
                        (y as f32 + (rng.gen::<f32>() - 0.5) * BLUR) / (self.height - 1) as f32;

                    let ray = self.camera.get_ray(u, v);

                    pixel_color += ray_color(&ray, &self.world, 0);
                }
                pixel_color /= SAMPLES_PER_PIXEL as f32;

                let index = y as usize * 3;
                color_column[index + 0] = pixel_color.x;
                color_column[index + 1] = pixel_color.y;
                color_column[index + 2] = pixel_color.z;
            }

            let mut img = img.lock().unwrap();
            for y in 0..self.height {
                let index = y as usize * 3;
                // let index = y as usize;
                let r = color_column[index + 0];
                let g = color_column[index + 1];
                let b = color_column[index + 2];

                let index = (x + y * self.width) as usize * 3;
                // img.set_pixel(x, y, Color::new(r, g, b, 1.0));
                img[index + 0] = r;
                img[index + 1] = g;
                img[index + 2] = b;
            }
        });

        self.data = Arc::try_unwrap(img).unwrap().into_inner().unwrap();
    }

    pub fn convert(&self) -> Image {
        // conversion
        let mut macroquad_image = Image::gen_image_color(
            self.width as u16,
            self.height as u16,
            Color::new(0.0, 0.0, 0.0, 1.0),
        );
        for x in 0..self.width {
            for y in 0..self.height {
                let index = (x + y * self.width) as usize * 3;
                let r = self.data[index + 0];
                let g = self.data[index + 1];
                let b = self.data[index + 2];

                let color = Color::new(r, g, b, 1.0);
                macroquad_image.set_pixel(x, y, color);
            }
        }

        macroquad_image
    }

    pub fn blend(&mut self, previous: &Vec<f32>, t: f32) {
        for x in 0..self.width {
            for y in 0..self.height {
                let index = (x + y * self.width) as usize * 3;
                let new_r = self.data[index + 0];
                let new_g = self.data[index + 1];
                let new_b = self.data[index + 2];

                let old_r = previous[index + 0];
                let old_g = previous[index + 1];
                let old_b = previous[index + 2];

                self.data[index + 0] = (old_r * t) + (new_r * (1.0 - t));
                self.data[index + 1] = (old_g * t) + (new_g * (1.0 - t));
                self.data[index + 2] = (old_b * t) + (new_b * (1.0 - t));
            }
        }
    }
}

fn ray_color(ray: &Ray, world: &World, depth: usize) -> Vec3 {
    if depth > RAY_DEPTH {
        return Vec3::ZERO;
    }

    let mut rng = thread_rng();
    let do_reflect: f32 = rng.gen();
    let do_refract: f32 = rng.gen();

    if let Some(hit) = world.hit(ray, 0.0, 100.0) {
        if hit.front {
            // return hit.normal + 0.5;
            // return Vec3::splat(hit.t / 3.0);
            // return hit.position;

            let material = hit.material;

            let mut diffusion_ray = if do_reflect > material.roughness {
                let Some(reflected) = reflect(ray.direction, hit.normal, material.roughness) else {return Vec3::ZERO};
                Ray::new(hit.position, reflected.normalize())
            } else {
                let Some(diffused) = diffuse(hit.normal) else {return Vec3::splat(0.0)};
                Ray::new(hit.position, diffused.normalize())
            };

            if do_refract < material.translucency {
                // let refracted = refract(diffusion_ray.direction, hit.normal, 1.0);
                let refracted = ray.direction + random_vec(-hit.normal) * material.roughness;
                diffusion_ray.direction = refracted;
            }
            let diffused = ray_color(&diffusion_ray, &world, depth + 1);
            return ((material.emission * Vec3::ONE) + ((1.0 - material.emission) * diffused))
                * material.color;
        }
    }

    let height = ray.direction().normalize().y;
    let t = height;
    return ((1.0 - t) * SKY_COLOR[0]) + (t * SKY_COLOR[1]);
}

fn reflect(ray: Vec3, normal: Vec3, roughness: f32) -> Option<Vec3> {
    let reflected = (ray - 2.0 * ray.dot(normal) * normal) + (random_vec(normal) * roughness);

    if reflected.dot(normal) > 0.0 {
        return Some(reflected);
    } else {
        return None;
    }
}

fn diffuse(normal: Vec3) -> Option<Vec3> {
    let fuzz = random_vec(normal);
    let diffused = normal + fuzz;
    if diffused.length() > EPSILON {
        Some(diffused)
    } else {
        None
    }
}

fn random_vec(normal: Vec3) -> Vec3 {
    let mut rng = thread_rng();
    let random = Vec3::new(
        (rng.gen::<f32>() - 0.5) * 2.0,
        (rng.gen::<f32>() - 0.5) * 2.0,
        (rng.gen::<f32>() - 0.5) * 2.0,
    );
    if random.dot(normal) > 0.0 {
        return random.normalize();
    } else {
        return -random.normalize();
    }
}
