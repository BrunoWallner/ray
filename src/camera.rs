use glam::{EulerRot, Mat3, Vec3};

use crate::Ray;

#[allow(dead_code)]
#[derive(Copy, Clone, Debug)]
pub struct Camera {
    origin: Vec3,
    pub yaw: f32,
    pub roll: f32,
    pub pitch: f32,
    lower_left_corner: Vec3,
    horizontal: Vec3,
    vertical: Vec3,
    focal_length: f32,
}
impl Camera {
    pub fn new(aspect_ratio: f32) -> Self {
        let viewport_height: f32 = 2.0;
        let viewport_width: f32 = viewport_height * aspect_ratio;
        let focal_length: f32 = 1.5;

        let yaw = 0.0;
        let pitch = 0.0;
        let roll = 0.0;
        let origin = Vec3::new(0.0, 1.0, 0.0);
        let horizontal = Vec3::new(viewport_width, 0.0, 0.0);
        let vertical = Vec3::new(0.0, viewport_height, 0.0);
        let lower_left_corner =
            origin - horizontal / 2.0 - vertical / 2.0 - Vec3::new(0.0, 0.0, focal_length);

        Self {
            origin,
            lower_left_corner,
            horizontal,
            vertical,
            focal_length,
            pitch,
            yaw,
            roll,
        }
    }

    pub fn get_ray(&self, u: f32, v: f32) -> Ray {
        let rotation = self.get_rotation_matrix();
        let direction = rotation
            * (self.lower_left_corner + u * self.horizontal + v * self.vertical - self.origin);

        Ray::new(self.origin, direction.normalize())
    }

    pub fn forward(&self) -> Vec3 {
        let rotation = self.get_rotation_matrix();
        rotation * Vec3::new(0.0, 0.0, 1.0)
    }

    pub fn right(&self) -> Vec3 {
        let rotation = self.get_rotation_matrix();
        rotation * Vec3::new(1.0, 0.0, 0.0)
    }

    pub fn shift(&mut self, direction: Vec3) {
        self.origin += direction;
        self.lower_left_corner = self.origin
            - self.horizontal / 2.0
            - self.vertical / 2.0
            - Vec3::new(0.0, 0.0, self.focal_length);
    }

    fn get_rotation_matrix(&self) -> Mat3 {
        Mat3::from_euler(EulerRot::YXZ, self.yaw, self.pitch, self.roll)
    }
}
