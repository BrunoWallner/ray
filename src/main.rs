pub mod aabb;
pub mod camera;
pub mod grid;
pub mod img;
pub mod material;
pub mod ray;
pub mod render;
pub mod shape;
pub mod world;

pub use camera::*;
pub use img::*;
pub use material::*;
pub use ray::*;
pub use render::*;
pub use shape::*;
use world::World;

use glam::Vec3;
use macroquad::prelude::{
    draw_texture_ex, get_frame_time, is_key_down, is_key_pressed, mouse_position_local, next_frame,
    screen_width, set_cursor_grab, show_mouse, Color, DrawTextureParams, FilterMode, KeyCode,
    Texture2D, Vec2,
};
use std::f32::EPSILON;

const IMAGE_BLENDING: f32 = 0.966;
const ASPECT_RATIO: f32 = 2.0;

const DEFAULT_WIDTH: u32 = 200;
const RENDER_WIDTH: u32 = 500;

#[macroquad::main("Ray")]
async fn main() {
    let camera = Camera::new(ASPECT_RATIO);

    let world = World::build();
    let mut renderer = Renderer::new(
        world,
        camera,
        DEFAULT_WIDTH,
        (DEFAULT_WIDTH as f32 / ASPECT_RATIO) as u32,
    );

    let mut image_blending: f32 = IMAGE_BLENDING;
    let mut render: bool = false;

    let mut cursor_grab = false;
    let mut mouse_shown = true;

    let mut last_mouse = Vec2::ZERO;

    loop {
        let dt = get_frame_time();

        let previous = renderer.data.clone();
        renderer.render();
        renderer.blend(&previous, image_blending);
        let img = renderer.convert();

        image_blending = IMAGE_BLENDING;

        let texture = Texture2D::from_image(&img);
        texture.set_filter(FilterMode::Nearest);
        let target_width = screen_width() as f32;
        draw_texture_ex(
            texture,
            0.0,
            0.0,
            Color::new(1.0, 1.0, 1.0, 1.0),
            DrawTextureParams {
                flip_y: true,
                dest_size: Some(Vec2::new(target_width, target_width / ASPECT_RATIO)),
                ..Default::default()
            },
        );

        // const speed: f32 = 0.05;
        let speed = dt;
        let forward = (-renderer.camera.forward() * Vec3::new(1.0, 0.0, 1.0)).normalize();
        let right = (renderer.camera.right() * Vec3::new(1.0, 0.0, 1.0)).normalize();
        let mut scene_changed = false;
        if is_key_down(KeyCode::W) {
            renderer.camera.shift(forward * speed);
            scene_changed = true;
        }
        if is_key_down(KeyCode::S) {
            renderer.camera.shift(-forward * speed);
            scene_changed = true;
        }
        if is_key_down(KeyCode::A) {
            renderer.camera.shift(-right * speed);
            scene_changed = true;
        }
        if is_key_down(KeyCode::D) {
            renderer.camera.shift(right * speed);
            scene_changed = true;
        }
        if is_key_down(KeyCode::Space) {
            renderer.camera.shift(Vec3::new(0.0, speed, 0.0));
            scene_changed = true;
        }
        if is_key_down(KeyCode::LeftShift) {
            renderer.camera.shift(Vec3::new(0.0, -speed, 0.0));
            scene_changed = true;
        }

        // mouse
        if cursor_grab {
            let mouse = mouse_position_local();
            let dt_mouse = last_mouse - mouse;
            if dt_mouse.abs().max_element() > EPSILON {
                renderer.camera.pitch += dt_mouse.y;
                renderer.camera.yaw += dt_mouse.x;
                scene_changed = true;
            }
            last_mouse = mouse;
        }
        if is_key_pressed(KeyCode::Escape) {
            mouse_shown = !mouse_shown;
            cursor_grab = !cursor_grab;
            show_mouse(mouse_shown);
            set_cursor_grab(cursor_grab);
        }

        if is_key_pressed(KeyCode::R) {
            // image_denoise = !image_denoise;
            render = !render;
            let width = match render {
                true => RENDER_WIDTH,
                false => DEFAULT_WIDTH,
            };
            renderer.set_dimension(width, (width as f32 / ASPECT_RATIO) as u32);
            image_blending = 0.0;
        }

        if scene_changed {
            image_blending = 0.0;
        }

        next_frame().await;
    }
}
