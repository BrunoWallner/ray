use glam::Vec3;

use image::{ImageFormat, ImageResult, Rgb, RgbImage};

pub struct Image {
    data: Vec<Vec3>,
    size: [u32; 2],
}
impl Image {
    pub fn new(size: [u32; 2]) -> Self {
        Self {
            data: vec![Vec3::new(0.0, 0.0, 0.0,); (size[0] * size[1]) as usize],
            size,
        }
    }

    pub fn size(&self) -> [u32; 2] {
        self.size
    }

    pub fn set(&mut self, x: usize, y: usize, color: Vec3) {
        let y = self.size[1] as usize - y - 1;
        self.data[x + y * self.size[0] as usize] = color;
    }
    pub fn set_column(&mut self, x: usize, color_column: &[Vec3]) {
        for (y, c) in color_column.iter().enumerate() {
            self.set(x, y, *c);
        }
    }

    pub unsafe fn get_unchecked(&self, x: usize, y: usize) -> &Vec3 {
        self.data.get_unchecked(x + y * self.size[0] as usize)
    }

    pub fn save(&self) -> ImageResult<()> {
        let mut img = RgbImage::new(self.size[0], self.size[1]);

        for x in 0..self.size[0] {
            for y in 0..self.size[1] {
                let color = unsafe { self.get_unchecked(x as usize, y as usize) };
                let [r, g, b] = color.to_array();
                let color_u8 = [(r * 255.0) as u8, (g * 255.0) as u8, (b * 255.0) as u8];
                img.put_pixel(x, y, Rgb(color_u8));
            }
        }

        img.save_with_format("out.png", ImageFormat::Png)
    }
}
