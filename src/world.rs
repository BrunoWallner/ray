use std::collections::HashMap;

use glam::Vec3;
use rand::{thread_rng, Rng};

use crate::{
    grid::Grid,
    ray::{Hit, Hittable, Ray},
    Material,
};

pub struct World {
    components: HashMap<String, Box<dyn Hittable + Send + Sync>>,
}

impl World {
    pub fn new() -> Self {
        Self {
            components: HashMap::default(),
        }
    }

    pub fn build() -> Self {
        let mut world = World::new();

        let mut rng = thread_rng();
        const START: i32 = -2;
        const END: i32 = 2;
        for x in START..END {
            for z in START..END {
                let mut grid = Grid::new(x as f32, 0.0, z as f32);
                for x in 0..32 {
                    for z in 0..32 {
                        for y in 0..32 {
                            if thread_rng().gen_bool(0.2) {
                                let material = Material::new(
                                    Vec3::new(rng.gen(), rng.gen(), rng.gen()),
                                    rng.gen(),
                                    0.0,
                                    if rng.gen_bool(0.2) { rng.gen() } else { 0.0 },
                                    // rng.gen(),
                                );

                                grid.set(x, y, z, Some(material));
                            }
                        }
                    }
                }
                let id = "grid".to_owned() + &x.to_string() + &z.to_string();
                world.add(&id, grid);
            }
        }

        world
    }

    pub fn get_mut(&mut self, id: &str) -> Option<&mut Box<dyn Hittable + Send + Sync + 'static>> {
        self.components.get_mut(&String::from(id))
    }

    pub fn add(&mut self, id: &str, hittable: impl Hittable + Send + Sync + 'static) {
        self.components.insert(String::from(id), Box::new(hittable));
    }

    pub fn clear(&mut self) {
        self.components.clear();
    }
}

impl Hittable for World {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<Hit> {
        let mut hit_record: Option<Hit> = None;

        for (_id, component) in self.components.iter() {
            if let Some(hit) = component.hit(ray, t_min, t_max) {
                if !hit.front {
                    continue;
                }
                if let Some(hit_record) = &mut hit_record {
                    if hit.t < hit_record.t {
                        *hit_record = hit;
                    }
                } else {
                    hit_record = Some(hit);
                }
            }
        }

        hit_record
    }
}
