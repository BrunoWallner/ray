/* FROM:
- https://www.youtube.com/watch?v=NbSee-XM7WA
- https://github.com/OneLoneCoder/Javidx9/blob/master/PixelGameEngine/SmallerProjects/OneLoneCoder_PGE_RayCastDDA.cpp
*/

use glam::{IVec3, Vec3};
use rand::{thread_rng, Rng};

use crate::{aabb::AABB, Hit, Hittable, Material, Ray};

const GRID_SIZE: usize = 32;
const GRID_INDEX_SIZE: usize = GRID_SIZE - 1;
const EDGE_THRESHOLD: f32 = 0.000001;

pub struct Grid {
    aabb: AABB,
    data: [Option<Material>; GRID_SIZE * GRID_SIZE * GRID_SIZE],
    center: Vec3,
}
impl Grid {
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        let aabb = AABB::new(
            Vec3::new(x, y, z) - Vec3::splat(0.5),
            Vec3::new(x, y, z) + Vec3::splat(0.5),
        );
        Self {
            aabb,
            data: [None; GRID_SIZE * GRID_SIZE * GRID_SIZE],
            center: Vec3::new(x, y, z),
        }
    }

    pub fn set(&mut self, x: usize, y: usize, z: usize, material: Option<Material>) {
        self.data[Grid::at(x, y, z)] = material;
    }

    pub fn get(&self, index: IVec3) -> &Option<Material> {
        let index = index.clamp(IVec3::ZERO, IVec3::splat(GRID_INDEX_SIZE as i32));
        self.data
            .get(Grid::at(
                index.x as usize,
                index.y as usize,
                index.z as usize,
            ))
            .unwrap()
    }

    fn at(x: usize, y: usize, z: usize) -> usize {
        x + y * GRID_SIZE + z * GRID_SIZE * GRID_SIZE
    }
}

impl Hittable for Grid {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<Hit> {
        let (local_start, root_to_aabb_t) = if let Some(hit) = self.aabb.hit(ray) {
            if hit.t > t_max || hit.t < t_min {
                return None;
            }

            ((hit.relative_position + 0.5), hit.t)
        } else if self.aabb.in_bound(ray.origin) {
            ((((ray.origin - self.center) / self.aabb.size()) + 0.5), 0.0)
        } else {
            return None;
        };

        let local_start = local_start * GRID_SIZE as f32;

        // visualisation of bounding box
        if thread_rng().gen_bool(0.0 /* opacity */) {
            return Some(Hit {
                position: ray.at(root_to_aabb_t),
                normal: aa_normal(&(ray.at(root_to_aabb_t) - self.aabb.center())),
                t: root_to_aabb_t,
                front: true,
                material: Material::new(ray.direction.abs(), 1.0, 0.0, 0.0),
            });
        }

        let step_size = Vec3::new(
            (1.0 + (ray.direction.y / ray.direction.x).powi(2)
                + (ray.direction.z / ray.direction.x).powi(2))
            .sqrt(),
            (1.0 + (ray.direction.x / ray.direction.y).powi(2)
                + (ray.direction.z / ray.direction.y).powi(2))
            .sqrt(),
            (1.0 + (ray.direction.x / ray.direction.z).powi(2)
                + (ray.direction.y / ray.direction.z).powi(2))
            .sqrt(),
        );

        let mut grid_position: IVec3 = local_start.as_ivec3();
        // hacky workaround to not skip edges upon aabb hit
        for i in 0..3 {
            if local_start[i] <= EDGE_THRESHOLD {
                grid_position[i] = -1;
            }
            if local_start[i] >= GRID_SIZE as f32 - EDGE_THRESHOLD {
                grid_position[i] = GRID_SIZE as i32;
            }
        }

        let grid_step = IVec3::new(
            ray.direction.x.signum() as i32,
            ray.direction.y.signum() as i32,
            ray.direction.z.signum() as i32,
        );

        // setup integer grid_step and initial ray_length
        let mut ray_length = Vec3::ZERO;
        for i in 0..3 {
            if grid_step[i] == -1 {
                ray_length[i] = (local_start[i] - grid_position[i] as f32) * step_size[i];
            } else if grid_step[i] == 1 {
                ray_length[i] = ((grid_position[i] + 1) as f32 - local_start[i]) * step_size[i];
            }
        }

        loop {
            // move
            let i = smallest_element(ray_length);
            grid_position[i] += grid_step[i];
            let distance = ray_length[i];
            ray_length[i] += step_size[i];

            // condition
            if grid_position.min_element() < 0
                || grid_position.max_element() > GRID_INDEX_SIZE as i32
            {
                break;
            }

            // test if hit
            if let Some(material) = self.get(grid_position) {
                let relative_distance = distance / GRID_SIZE as f32;
                let absolute_distance = root_to_aabb_t + relative_distance;
                let absolute_position = ray.at(absolute_distance);

                let relative_position = local_start + ray.direction * distance;
                let from_center = relative_position - (grid_position.as_vec3() + 0.5);
                let normal = aa_normal(&from_center);
                // let front = local_start.as_ivec3() != grid_position;
                let front = true;
                if front {
                    return Some(Hit {
                        position: absolute_position,
                        normal,
                        t: absolute_distance,
                        front,
                        material: *material,
                    });
                }
            }
        }

        None
    }
}

fn smallest_element(vec: Vec3) -> usize {
    if vec.x < vec.y && vec.x < vec.z {
        0
    } else if vec.y < vec.x && vec.y < vec.z {
        1
    } else if vec.z < vec.x && vec.z < vec.y {
        2
    } else {
        0
    }
}

fn aa_normal(vec: &Vec3) -> Vec3 {
    let x = vec.x.abs();
    let y = vec.y.abs();
    let z = vec.z.abs();

    let x2 = if x > z && x > y {
        1.0 * vec.x.signum()
    } else {
        0.0
    };
    let y2 = if y > z && y > x {
        1.0 * vec.y.signum()
    } else {
        0.0
    };
    let z2 = if z > y && z > x {
        1.0 * vec.z.signum()
    } else {
        0.0
    };

    Vec3::new(x2, y2, z2)
}
