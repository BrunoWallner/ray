use glam::Vec3;

#[derive(Copy, Clone, PartialEq)]
pub struct Material {
    pub color: Vec3,
    pub roughness: f32,
    pub translucency: f32,
    pub emission: f32,
}

impl Material {
    pub fn new(color: Vec3, roughness: f32, translucency: f32, emission: f32) -> Self {
        Self {
            color,
            roughness,
            translucency,
            emission,
        }
    }
}
